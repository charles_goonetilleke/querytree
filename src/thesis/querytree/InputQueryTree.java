package thesis.querytree;

import thesis.querytree.structure.BaseNode;
import thesis.querytree.structure.GroupByOperatorNode;
import thesis.querytree.structure.OperatorNode;
import thesis.querytree.structure.ProjectionOperatorNode;
import thesis.querytree.structure.SelectionOperatorNode;

public class InputQueryTree {
    public String sql;
    public OperatorNode root;

    public InputQueryTree(OperatorNode root) {
        this.root = root;
    }
    
    public InputQueryTree(String sql, OperatorNode root) {
        this.sql = sql;
        this.root = root;
    }
    
    public InputQueryTree(String sql) {
        this.sql = sql;
    }
    
    public String getSql(){
        return sql;
    }
    
    public void setSql(String sql){
        this.sql = sql;
    }

    @Override
    public String toString() {
        return this.root.toString();
    }
    
    public String displayData(){
        BaseNode node = root;
        String s = "";
        while(node instanceof SelectionOperatorNode || node instanceof GroupByOperatorNode || node instanceof ProjectionOperatorNode){
            s += ((OperatorNode)node).condition.toString();
            s += "\n";
            node = node.children.get(0);
        }
        
        return s;
    }
}
