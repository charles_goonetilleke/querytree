package thesis.querytree;

import thesis.querytree.enums.StatusEnum;
import thesis.querytree.structure.BaseNode;
import thesis.querytree.structure.GroupByOperatorNode;
import thesis.querytree.structure.OperatorNode;
import thesis.querytree.structure.ProjectionOperatorNode;
import thesis.querytree.structure.SelectionOperatorNode;

public class MaterializedViewQueryTree implements Comparable<MaterializedViewQueryTree> {   
    public int id;  // unique identifier
    public OperatorNode root;
    public int currentSessionHits = 0;    
    public StatusEnum status;
    public String name;
    
    public static int sequence = 0;

    public MaterializedViewQueryTree(OperatorNode root) {
        this.id = ++sequence;
        this.name = "MV"+sequence;
        this.root = root;
        this.status = StatusEnum.ACTIVE;
    }
    
    public MaterializedViewQueryTree(String name, OperatorNode root) {
        this.name = name;
        this.id = ++sequence;
        this.root = root;
        this.status = StatusEnum.ACTIVE;
    }

    public void ResetHits() {
        this.currentSessionHits = 0;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    public String displayData(){
        BaseNode node = root;
        String s = "";
        while(node instanceof SelectionOperatorNode || node instanceof GroupByOperatorNode || node instanceof ProjectionOperatorNode){
            s += ((OperatorNode)node).condition.toString();
            s += "\n";
            node = node.children.get(0);
        }
        
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass() == MaterializedViewQueryTree.class) {
            return this.id == ((MaterializedViewQueryTree) o).id;
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.id;
        return hash;
    }

    @Override
    public int compareTo(MaterializedViewQueryTree o) {
        if (this.id == o.id) {
            return 0;
        } else if (this.id > o.id) {
            return 1;
        } else {
            return -1;
        }
    }
}