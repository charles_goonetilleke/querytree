package thesis.querytree.structure;

import thesis.querytree.structure.conditions.GroupByCondition;
import thesis.querytree.enums.OperatorNodeTypeEnum;

public class GroupByOperatorNode extends OperatorNode {
    
    public GroupByOperatorNode(GroupByCondition aggregateCondition, BaseNode child) throws Exception{
        super(OperatorNodeTypeEnum.GROUPBY, aggregateCondition, child);
    }
}