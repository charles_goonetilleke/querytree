package thesis.querytree.structure.conditions;

public interface Condition {
    
    @Override
    public String toString();
}