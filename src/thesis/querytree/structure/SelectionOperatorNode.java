package thesis.querytree.structure;

import thesis.querytree.structure.conditions.SelectionCondition;
import thesis.querytree.enums.OperatorNodeTypeEnum;

public class SelectionOperatorNode extends OperatorNode {
    
    public SelectionOperatorNode(SelectionCondition selectionCondition, BaseNode child) throws Exception{
        super(OperatorNodeTypeEnum.SELECTION, selectionCondition, child);
    }
}