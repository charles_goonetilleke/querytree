package thesis.querytree.structure;

import thesis.querytree.structure.conditions.ProjectionCondition;
import thesis.querytree.enums.OperatorNodeTypeEnum;

public class ProjectionOperatorNode extends OperatorNode {

    public ProjectionOperatorNode(ProjectionCondition projectionCondition, BaseNode child) throws Exception {
        super(OperatorNodeTypeEnum.PROJECTION, projectionCondition, child);
    }
}
